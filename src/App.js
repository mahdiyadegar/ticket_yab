import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import React, { Component } from 'react';
import Personform from "./_forms/Person"
import Signupform from "./_forms/person_type"
import Pilotform from "./_forms/pilot"
import Loginform from "./_forms/login"
import Homepage from "./_forms/homepage"
import Register from "./_forms/register"
import Airline from "./_forms/airline_signup"
import Airline_home from "./_forms/airline_home"


class App extends Component {
  render() {
    return (
            <Router>
                <div className="App">
                    {/*<Navbar />*/}
                    <div className="contain">
                        <Switch>
                            <Route path="/person" component={Personform} />
                            <Route path="/signup" component={Signupform} />
                            <Route path="/pilot" component={Pilotform} />
                            <Route path="/login" component={Loginform} />
                            <Route path="/home" component={Homepage} />
                            <Route path="/register" component={Register} />
                            <Route path="/airline" component={Airline} />
                            <Route path="/airline_home" component={Airline_home} />
                            {/* <Route path="/picksquad" component={PickSquadContainer} />
                            <Route path="/manageTeam" component={ManageTeam}/> */}
                        </Switch>
                    </div>
                </div>
            </Router>
    )
  }
}

export default App;
