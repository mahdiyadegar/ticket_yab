import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link , Redirect} from "react-router-dom";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";



import "react-responsive-ui/style.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import {TextInput} from "react-responsive-ui";


class Personform extends Component {

    state = {
        type:""
    };

    handleChanger(e) {
        this.setState({ type:"done" });
        console.log("fiiiiii")
    }
    handleChanger2(e) {
        this.setState({ type:"air_done" });
        console.log("ad")
    }
    render() {
        if (this.state.type == "done") return <Redirect to={{ pathname: '/home' }} />
        if (this.state.type == "air_done") return <Redirect to={{ pathname: '/airline_home' }} />
        return (
            <div className="login-center container py-3">
                <div className="row justify-content-center">
                    <p className="login-title mt-3"> Welcome to our website </p>
                </div>
                <hr/>
                <div className="row justify-content-center">
                    <p className="login-title mt-3"> login </p>
                </div>
                <hr/>
                <div className="row justify-content-center mb-5">
                    <TextInput
                        type="text"
                        label="SSN"
                    />
                </div>
                <div className="row justify-content-center mb-5">
                    <TextInput
                        type="text"
                        label="Password"
                    />
                </div>
                <div className="row justify-content-center sign-in-button">
                        <Button id="button" className=" button-submit-login" type="submit" onClick={this.handleChanger.bind(this)}
                                variant="success"
                                size="md">
                            Login
                        </Button>

                </div>
                <br />
                <div className="row justify-content-center sign-in-button">
                        <Button id="button" className=" button-submit-login" type="submit" onClick={this.handleChanger2.bind(this)}
                                variant="success"
                                size="md">
                            Login-airline
                        </Button>

                </div>

            </div>
        )
    }
    
}

export default Personform;