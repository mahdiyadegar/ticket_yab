import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link , Redirect} from "react-router-dom";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";
import Table from 'react-bootstrap/Table'
import Carousel from 'react-bootstrap/Carousel'




import "react-responsive-ui/style.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import {TextInput} from "react-responsive-ui";


class Personform extends Component {

    state = {
        type:""
    };

    handleChanger(e) {
        this.setState({ type:"reg" });
        console.log("fiiiiii")
    }
    render() {
        if (this.state.type == "reg") return <Redirect to={{ pathname: '/register' }} />
        return (
            <div className="login-center container py-3">
                <div className="row justify-content-center">
                    <p className="login-title mt-3"> Ticket yab </p>
                </div>
                <hr/>
                <div className="row justify-content-center">
                    <p className="login-title mt-3"> find your ticket easily </p>
                </div>  
                <hr/>
                <Carousel>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="download1.svg"
                        alt="First slide"
                        />
                        <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="download2.svg"
                        alt="Third slide"
                        />

                        <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="download3.svg"
                        alt="Third slide"
                        />

                        <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    </Carousel>
                <hr/>
                <br />
                <div className="row">
                    <div className="col-sm-3">
                        search your wanted ticket
                    </div>
                    <div className="col-sm-6">
                    <TextInput
                        type="text"
                        label="SSN"
                        />
                    </div>
                    <div className="col-sm-3">
                    <Button id="button" className=" button-submit-login" type="submit" onClick={this.handleChanger.bind(this)}
                            variant="success"
                            size="md">
                        search
                    </Button>
                    </div>
                </div>
                <br />
                <hr/>
                
                {
                    [1,2,3].map ( (n) => {
                        return <div>
                        <hr/>
                        <br />
                        <div className="row">
                            <div className="col-sm-3">
                                <div className="row justify-content-center sign-in-button">
                                                    <Button id="button" className=" button-submit-login" type="submit" onClick={this.handleChanger.bind(this)}
                                                            variant="success"
                                                            size="md">
                                                        register
                                                    </Button>
        
                                            </div>
                            </div>
                            <div className="col-sm-9">
                                <div className="row justify-content-center sign-in-button">
                                <Table striped bordered hover>
                                <thead>
                                    <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td>1</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    </tr>
                                    <tr>
                                    <td>2</td>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    </tr>
                                    <tr>
                                    <td>3</td>
                                    <td colSpan="2">Larry the Bird</td>
                                    <td>@twitter</td>
                                    </tr>
                                </tbody>
                                </Table>                    
        
                                </div>
                            </div>
                        </div>
                        <br/>
                        <hr/>
                        </div>
                    })
                }
                

                {/* <div className="row justify-content-center mb-5">
                    <TextInput
                        type="text"
                        label="SSN"
                    />
                </div>
                <div className="row justify-content-center mb-5">
                    <TextInput
                        type="text"
                        label="Password"
                    />
                </div>
                <div className="row justify-content-center sign-in-button">
                        <Button id="button" className=" button-submit-login" type="submit" onClick={this.handleChanger.bind(this)}
                                variant="success"
                                size="md">
                            Login
                        </Button>

                </div> */}

            </div>
        )
    }
    
}

export default Personform;