import React, { Component } from "react";

import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";



import "react-responsive-ui/style.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import {TextInput} from "react-responsive-ui";


class Personform extends Component {
    render() {
        return (
            <div className="login-center container py-3">
                <div className="row justify-content-center">
                    <p className="login-title mt-3"> Airline </p>
                </div>
                <hr/>
                <div className="row direct-to-signup-box">
                    <div className="col-sm-6">
                        <div className="row justify-content-center guide-to-signup ">
                            Register your airline to find customers faster than ever
                        </div>
                    </div>

                </div>
                <hr/>
                <div className="row align-items-center">
                    <div className="col-sm-6 col-md-6">
                        <div className="row justify-content-center align-items-center">
                            <form>
                                <div className="container">
                                    <div className="row justify-content-center mb-5">
                                        <TextInput
                                            type="text"
                                            label="international"
                                        />
                                    </div>
                                    <div className="row justify-content-center mb-5">
                                        <TextInput
                                            type="text"
                                            label="Name"
                                        />
                                    </div>
                                    {/* <div className="row justify-content-center mb-5">
                                    <DropdownButton as={ButtonGroup} title="Gender" id="bg-nested-dropdown">
                                        <Dropdown.Item eventKey="1">Male</Dropdown.Item>
                                        <Dropdown.Item eventKey="2">Female</Dropdown.Item>
                                    </DropdownButton>
                                    </div> */}
                                    {/* <div className="row justify-content-center mb-5">
                                        <TextInput
                                            type="text"
                                            label="country"
                                        />
                                    </div> */}
                                    <div className="row justify-content-center sign-in-button">
                                            <Button id="button" className=" button-submit-login" type="submit"
                                                    variant="success"
                                                    size="md">
                                                SUBMIT
                                            </Button>

                                    </div>
                                </div>



                            </form>
                        </div>

                    </div>
                </div>


            </div>
        )
    }
    
}

export default Personform;