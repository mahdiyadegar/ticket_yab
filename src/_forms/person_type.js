import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link , Redirect} from "react-router-dom";
import Button from "react-bootstrap/Button";
import DropdownButton from "react-bootstrap/DropdownButton";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";



import "react-responsive-ui/style.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import {TextInput} from "react-responsive-ui";


class Personform extends Component {

    state = {
        type:""
    };

    handleChanger(e) {
        this.setState({ type:"pilot" });
        console.log("fiiiiii")
    }
    handleChanger2(e) {
        this.setState({ type:"pass" });
        console.log("fdddd")
    }
    handleChanger3(e) {
        this.setState({ type:"airline" });
        console.log("air")
    }
    render() {
        if (this.state.type == "pilot") return <Redirect to={{ pathname: '/pilot' }} />
        if (this.state.type == "pass") return <Redirect to={{ pathname: '/person' }} />
        if (this.state.type == "airline") return <Redirect to={{ pathname: '/airline' }} />
        return (
            <div className="login-center container py-3">
                <div className="row justify-content-center">
                    <p className="login-title mt-3"> PERSON </p>
                </div>
                <hr/>
                <div className="row justify-content-center">
                    <p className="login-title mt-3"> What type of account do you want ? </p>
                </div>
                <hr/>
                <div className="row">
                    <div className="col-sm-4">
                        <div className="row justify-content-center sign-in-button">
                                            <Button id="button" className=" button-submit-login" type="submit" onClick={this.handleChanger.bind(this)}
                                                    variant="success"
                                                    size="md">
                                                Pilot
                                            </Button>

                                    </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="row justify-content-center sign-in-button">
                                            <Button id="button" className=" button-submit-login" type="submit" onClick={this.handleChanger2.bind(this)}
                                                    variant="success"
                                                    size="md">
                                                Passenger
                                            </Button>

                                    </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="row justify-content-center sign-in-button">
                                            <Button id="button" className=" button-submit-login" type="submit" onClick={this.handleChanger3.bind(this)}
                                                    variant="success"
                                                    size="md">
                                                airline
                                            </Button>

                                    </div>
                    </div>
                </div>
                {/* <div className="row direct-to-signup-box">
                    <div className="col-sm-6">
                        <div className="row justify-content-center guide-to-signup " style={{textAlign:"center"}}>
                            Register a person
                        </div>
                    </div>

                </div>
                <hr/>
                <div className="row align-items-center">
                    <div className="col-sm-6 col-md-6">
                        <div className="row justify-content-center align-items-center">
                            <form>
                                <div className="container">
                                    <div className="row justify-content-center mb-5">

                                        <TextInput
                                            type="text"
                                            label="First Name"
                                        />


                                    </div>

                                    <div className="row justify-content-center mb-5">
                                        <TextInput
                                            type="text"
                                            label="Last Name"
                                        />
                                    </div>
                                    <div className="row justify-content-center mb-5">
                                        <TextInput
                                            type="text"
                                            label="SSN"
                                        />
                                    </div>
                                    <div className="row justify-content-center mb-5">
                                    <DropdownButton as={ButtonGroup} title="Gender" id="bg-nested-dropdown">
                                        <Dropdown.Item eventKey="1">Male</Dropdown.Item>
                                        <Dropdown.Item eventKey="2">Female</Dropdown.Item>
                                    </DropdownButton>
                                    </div>
                                    <div className="row justify-content-center mb-5">
                                        <TextInput
                                            type="text"
                                            label="Address"
                                        />
                                    </div>
                                    <div className="row justify-content-center sign-in-button">
                                            <Button id="button" className=" button-submit-login" type="submit"
                                                    variant="success"
                                                    size="md">
                                                SUBMIT
                                            </Button>

                                    </div>
                                </div>



                            </form>
                        </div>

                    </div>
                </div> */}


            </div>
        )
    }
    
}

export default Personform;